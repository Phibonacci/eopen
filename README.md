Simple Bourne shell script that opens `pdf` files, from a specified directory and it's sub directorys, matching keywords.

#Usage

`usage: eopen word [[word] ...]`

#Behavior

If several files match it prints a list of choices and wait for you to type a matching number from the list.
If you are not in a shell and several files match, it opens a prompt.

#Install

Edit the first variable of the script :

`pdfreader` your favorite pdf reader.

`yourpath` the path of your file (the more files it has, the slower it is)

`term` your terminal emulator (xterm/koncole etc...)

Make is executable:

`chmod +x eopen`

Move it to a bin PATH :

`sudo mv eopen /usr/bin/`

# More

It can be easily modified to open any kind of file.
Not case sensitive.